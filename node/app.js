const express = require('express');

const { build } = require('./build.js')
const app = express();

app.use(express.static('dist'))
app.get('/',build)
app.get('/index.html',build)
app.get('/index', build)
app.get('/:id.html', build)
app.get('/:id', build)

app.listen(80);