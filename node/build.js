const fetch = require('node-fetch');
const shell = require('shelljs')


//check env variable

const alphaSort = (a, b) => {
    if(a.toLowerCase() < b.toLowerCase()) { return -1; }
    if(a.toLowerCase() > b.toLowerCase()) { return 1; }
    return 0;
}

module.exports = {
    build: async (req,res) =>{
        
        const codiLink = req.params.id || process.env.ID_CODI || 'v2CLkfkBQBGmGXPqvMC6yA'
        const notes = `dist/notes-${codiLink}.md`
        const key = `dist/key-${codiLink}.md`
        const isNotIndex = (req.params.id)
        const path = isNotIndex ? `dist/${codiLink}.html` : `dist/index.html`
        
        /*
        TODO se protéger des méchants hackeurs
        
        let allowedUsers = ['marcello','Nicolas','arthur']
        let check = await fetch(`https://demo.codimd.org/${codiLink}/authors`)
        check = await check.json()
        if(!allowedUsers.includes(check.author)){
            return ('je suis pas content')
        }
        */

        let codiMD = await fetch(`https://demo.hedgedoc.org/${codiLink}/download`)
        codiMD = await codiMD.text()
        shell.echo(codiMD).to(notes)

        shell.cat('/dev/null').to(key)
        const categories = codiMD.match(/@[A-Za-z0-9_-]*:[A-Za-z0-9_-]*/g)
        if(categories){
            //Unique + remove @ and :\w
            const catlist = [... new Set(categories.sort(alphaSort).map(s => s.split(':')[0].substring(1)))]

            for(cat of catlist){
                shell.echo(`- ### ${cat.charAt(0).toUpperCase() + cat.slice(1)}`).toEnd(key)
                const regex = new RegExp(`@${cat}:\w*`,'g')
                shell.echo('- '+[... new Set(categories)].filter(c=>c.match(regex)).sort(alphaSort).join('\n- ')+'\n').toEnd(key)
            }
        }
        const keywords = codiMD.match(/@(?![A-Za-z0-9_-]*:)[A-Za-z0-9_-]*/g)
        if(keywords){
            shell.echo('- ### Keywords').toEnd(key)
            shell.echo('- '+[... new Set(keywords.sort(alphaSort))].join('\n- ')+'\n').toEnd(key)
        }
        shell.sed('-i','@\([A-Za-z0-9_-]*\):\([A-Za-z0-9_-]*\)','[[$2](#$1-$2)]{.$1-$2}',key)
        shell.sed('-i','@\([A-Za-z0-9_-]*\)','[[$1](#keyword-$1)]{.keyword-$1}',key)

        shell.exec(`pandoc ${key} -o dist/key-${codiLink}.html`)

        shell.sed('-i',/@([A-Za-z0-9_-]*):([A-Za-z0-9_-]*)/g,'[[$2](#$1-$2)]{.$1-$2}',notes)
        shell.sed('-i',/@([A-Za-z0-9_-]*)/g,'[[$1](#keyword-$1)]{.keyword-$1}',notes)


        const variables = isNotIndex ? `--variable=ID:${codiLink} --variable=IDbuild:${codiLink}` : `-V ID=${codiLink} --variable=IDbuild:index` 
        shell.exec(`pandoc --standalone --section-divs --toc --css=preview.css --toc-depth=2 --template=data/template.html5 --lua-filter=data/date.lua -f markdown -t html5 ${variables} ${notes} -o ${path}`).code

        shell.sed('-i', "t0t0", `https://demo.hedgedoc.org/${codiLink}?edit`, path)
        shell.sed('-i', "<script\/>", `<script>
        $("#motcles").load("key-${codiLink}.html",function(){
            $(document).ready(function(){
                function select (event) { 

                var myClass = event.currentTarget.className
                // alert(myClass);
                if(myClass){
                        $("section:not(:has(."+myClass+"))").css({"display":"none"});
                        $('.'+myClass).css("border", "3px solid red");
                        $('.'+myClass).one('click', unselect);
                           };
                                        } 
                function unselect (event) { 
                var myClass = event.currentTarget.className
                // alert(myClass);
                if(myClass){
                        $("section:not(:has(."+myClass+"))").css({"display":"block"});
                        $('.'+myClass).css("border", "0px");
                        $('.'+myClass).one('click', select);
                };
                } 
                $("span").click(select);
                });
        });</script>`, path)

        res.redirect(req.params.id ? `/${codiLink}.html` : `/index.html`)
    }

}
