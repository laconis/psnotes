# psNotes

Lowtech notes pour Publishing Sphere

## Requirement

- docker
- pandoc 2.*

The master branch implements a docker+nodejs solution, thanks to @pookmook. A simple bash version doing the same thing is available on branch `simpleBash`.

## Getting Started

- create a pad on [Hedgedoc.md](https://demo.hedgedoc.org)
- add a metadata block with a title line at the beginning of the document (don't forget the `---` at the beginning and the end):
  ```yaml
  ---
  title: Title for those notes
  ---
  ```
- in `docker-compose.yaml`, edit the variable `ID_CODI` with the id of your pad.
  For instance : `https://demo.hedgedoc.org/aEp-bIKuRbeTRy1yxXxsEA?edit` > `ID_CODI=aEp-bIKuRbeTRy1yxXxsEA`
- build and start the docker: `docker-compose up -d --build`
- the docker is accessible on port `8082` : `firefox http://localhost:8082/`
- edit your pad on Codi.md and press the `reload` button to refresh your page.

That's it.

## psNotes syntax

- Each note is separated by a markdown level 2 title : `## My title`
- Each keyword is identified by a `@` sign : `@concept`
- keywords can be categorized with `@category:keyword` : `@author:toto`

---

[![WTFLP](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-2.png)](http://www.wtfpl.net/)
